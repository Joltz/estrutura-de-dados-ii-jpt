package br.edu.udc.ed.tabela.produto;

import br.edu.udc.ed.vetor.Vetor;

@SuppressWarnings("hiding")
public class TabelaProduto<Produto> {
	private Vetor<Vetor<Produto>> tabelaProduto = new Vetor<Vetor<Produto>>();

	private static final short TAMANHO_MINIMO = 10;

	private int quantidade = 0;

	public TabelaProduto() {
		for (int i = 0; i < TAMANHO_MINIMO; i++) {
			final Vetor<Produto> lista = new Vetor<Produto>();
			this.tabelaProduto.adiciona(lista);
		}
	}

	private int calculaIndice(Produto objeto) {
		int codigoDeEspalhamento = objeto.hashCode();

		codigoDeEspalhamento = Math.abs(codigoDeEspalhamento);

		return codigoDeEspalhamento % this.tabelaProduto.tamanho();
	}

	public void adiciona(Produto objeto) {
		if (!this.contem(objeto)) {
			
			final int indice = this.calculaIndice(objeto);
			this.tabelaProduto.obtem(indice).adiciona(objeto);
			this.quantidade++;
		}
	}

	public void remove(Produto objeto) {
		if (this.contem(objeto)) {
			final int indice = this.calculaIndice(objeto);
			final Vetor<Produto> lista = this.tabelaProduto.obtem(indice);

			for (int i = 0; i < lista.tamanho(); i++) {
				if (lista.obtem(i).equals(objeto)) {
					lista.remove(i);
					break;
				}
			}

			this.quantidade--;
		} else {
			throw new RuntimeException("Objeto " + objeto + " n�o existente.");
		}
	}

	public boolean contem(Produto objeto) {
		final int indice = this.calculaIndice(objeto);
		return this.tabelaProduto.obtem(indice).contem(objeto);
	}

	public Vetor<Produto> todas() {
		final Vetor<Produto> todosObjetos = new Vetor<Produto>();

		for (int i = 0; i < this.tabelaProduto.tamanho(); i++) {
			final Vetor<Produto> objetos = this.tabelaProduto.obtem(i);

			for (int j = 0; j < objetos.tamanho(); j++) {
				final Produto objeto = objetos.obtem(j);
				todosObjetos.adiciona(objeto);
			}
		}

		return todosObjetos;
	}

	public int tamanho() {
		return this.quantidade;
	}
}
