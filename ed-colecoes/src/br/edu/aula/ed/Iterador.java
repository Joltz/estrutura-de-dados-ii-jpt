package br.edu.aula.ed;

public interface Iterador<T> {
	boolean temProximo();
	T proximo();
}
