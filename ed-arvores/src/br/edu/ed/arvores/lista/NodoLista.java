package br.edu.ed.arvores.lista;

import br.edu.ed.arvores.NodoAbstrato;
import br.edu.udc.ed.vetor.Vetor;

public class NodoLista<E> extends NodoAbstrato<E> {
	
	protected Vetor<NodoAbstrato<E>> filhos = new Vetor<>();
	protected int tamanhoArvore = 0;
	
	public NodoLista(E elemento, NodoAbstrato<E> nodoPai) {
		super(elemento, nodoPai);
	}
	
	public NodoLista(E elementoRaiz) {
		super(elementoRaiz);
		this.tamanhoArvore = 1;
	}
	
	@Override
	public NodoLista<E> adicionar(E elemento) {
		return this.adicionar(elemento, this.filhos.tamanho());
	}
	
	public NodoLista<E> adicionar(E elemento, int ordem) {
		final NodoLista<E> nodoFilho = new NodoLista<E>(elemento, this);
		this.filhos.adiciona(ordem, nodoFilho);
		
		final NodoLista<E> raiz = (NodoLista<E>) super.getRaiz();
		raiz.tamanhoArvore++;
		return (NodoLista<E>) nodoFilho;
	}

	@Override
	public int altura() {
		return 0;
	}

	@Override
	public int tamanhoArvore() {
		return this.raiz.tamanhoArvore();
	}

	@Override
	public Vetor<NodoAbstrato<E>> getFilhos() {
		return this.filhos;
	}
	
}
