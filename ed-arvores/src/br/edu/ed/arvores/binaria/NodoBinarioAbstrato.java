package br.edu.ed.arvores.binaria;

import br.edu.ed.arvores.NodoAbstrato;

public abstract class NodoBinarioAbstrato<E> extends NodoAbstrato<E>  {
	
	public NodoBinarioAbstrato(E elemento, NodoBinarioAbstrato<E> nodoPai) {
		super(elemento, nodoPai);
	}

	public NodoBinarioAbstrato(E elementoRaiz) {
		super(elementoRaiz);
	}
	
	public abstract NodoBinarioAbstrato<E> getEsquerdo();
	public abstract NodoBinarioAbstrato<E> getDireito();
	
	public boolean completo() {
		return this.getFilhos().tamanho() == 0 || this.getFilhos().tamanho() == 2;
	}
	
	public boolean incompleto() {
		return this.completo();
	}

}
