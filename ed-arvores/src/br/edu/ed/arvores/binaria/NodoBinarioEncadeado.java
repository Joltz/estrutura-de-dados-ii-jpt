package br.edu.ed.arvores.binaria;

import br.edu.ed.arvores.NodoAbstrato;
import br.edu.udc.ed.vetor.Vetor;

public class NodoBinarioEncadeado<E> extends NodoBinarioAbstrato<E> {
	
	protected NodoBinarioEncadeado<E> esquerdo;
	protected NodoBinarioEncadeado<E> direito;
	protected int tamanhoArvore = 0;

	public NodoBinarioEncadeado(E elementoRaiz, NodoBinarioEncadeado<E> nodoPai) {
		super(elementoRaiz, nodoPai);
	}
	
	public NodoBinarioEncadeado(E elementoRaiz) {
		super(elementoRaiz);
		this.tamanhoArvore = 1;
	}

	@Override
	public NodoBinarioAbstrato<E> getEsquerdo() {
		return this.esquerdo;
	}

	@Override
	public NodoBinarioAbstrato<E> getDireito() {
		return this.direito;
	}

	@Override
	public int altura() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int tamanhoArvore() {
		return this.tamanhoArvore;
	}

	@Override
	public NodoAbstrato<E> adicionar(E elemento) {
		final NodoBinarioEncadeado<E> nodoFilho = new NodoBinarioEncadeado<E>(elemento, this);
		
		if(this.getEsquerdo() == null) {
			this.esquerdo = nodoFilho;
		} else if(this.getDireito() == null) {
			this.direito = nodoFilho;
		} else {
			throw new IllegalArgumentException("J� cont�m um filho esquerdo e direito.");
		}
		
		final NodoBinarioEncadeado<E> raiz = (NodoBinarioEncadeado<E>) super.getRaiz();
		raiz.tamanhoArvore++;
		return (NodoBinarioEncadeado<E>) nodoFilho;
	}

	@Override
	public Vetor<NodoAbstrato<E>> getFilhos() {
		final Vetor<NodoAbstrato<E>> filhos = new Vetor<>();
		
		if(this.getEsquerdo() != null) {
			filhos.adiciona(this.getEsquerdo());
			
			if(this.getDireito() != null) {
				filhos.adiciona(this.getDireito());
			}
			
		} else if(this.getDireito() != null) {
			throw new IllegalStateException("Arvore impropria. So pode haver um no na direita.");
		}
		
		return filhos;
	}

}
