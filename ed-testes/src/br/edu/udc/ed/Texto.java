package br.edu.udc.ed;

public class Texto {

	private String texto;

	public Texto(String texto) {
		this.texto = texto;
	}

	public String converterEmMinusculo() {
		return texto.toLowerCase();
	}

	public int indiceDe(String caracteres) {
		return this.texto.indexOf(caracteres);
	}

	public String subText(int inicio, int fim) {
		return this.texto.substring(inicio, fim);
	}

	public boolean terminaCom(String caracteres) {
		return this.texto.endsWith(caracteres);
	}

	public String eliminarEspacos() {
		return this.texto.replace(" ", "");
	}

	public char caracterNoIndice(int indice) {
		return this.texto.charAt(indice);
	}

	public String[] separarPor(String separador) {
		return this.texto.split(separador);
	}

}
