package br.edu.udc.ed.testes;

import org.junit.Assert;
import org.junit.Test;

import br.edu.udc.ed.Texto;

public class TextoTeste {
	
	//converterEmMinusculo
	@Test
	public void converterEmMinusculoDevePassar(){
		final String nome = "JOAO PEDRO TAGLIABOA";
		final Texto texto = new Texto(nome);
		
		final String nomeMinusculo = texto.converterEmMinusculo();
		
		Assert.assertEquals("N�o converteu em minusculo.", nome.toLowerCase(), nomeMinusculo);
		
	}
	
	@Test
	public void converterEmMinusculoDevePassarComNumeros(){
		final String numeros = "0101010101010101010";
		final Texto texto = new Texto(numeros);
		
		final String numerosConvertidos = texto.converterEmMinusculo();
		
		Assert.assertEquals("N�o deveria converter.", numeros, numerosConvertidos);
		
	}
	
	@Test(expected=NullPointerException.class)
	public void converterEmMinusculoDeveFalhar(){
		final Texto texto = new Texto(null);
		
		texto.converterEmMinusculo();
		
		Assert.fail("Deveria ter retornado um nullpointer exception.");
		
	}
	
	
	//indiceDe
	
	@Test
	public void indiceDeDevePassar(){
		final Texto texto = new Texto("Uma Palavra");
		int indice = texto.indiceDe("P");
		
		Assert.assertEquals("O indice esta invalido.", 4, indice);
		
	}
	
	@Test
	public void indiceDeDeveRetornarMenos1(){
		final Texto texto = new Texto("Uma Palavra");
		int indice = texto.indiceDe("X");
		
		Assert.assertEquals("O indice esta invalido.", -1, indice);
		
	}
	
	@Test
	public void indiceDeDeveRetornarIndice5(){
		final Texto texto = new Texto("10010�100011010100100");
		int indice = texto.indiceDe("�");
		
		Assert.assertEquals("O indice esta invalido.", 5, indice);
		
	}
	
	@Test
	public void indiceDeDeveEncontrarMinusculo(){
		final Texto texto = new Texto("MAIUSCULO");
		final int indice = texto.indiceDe("i");
		
		Assert.assertEquals("Nao deveria ter encontrado um indice.", -1, indice);
		
	}
	
	
	//subTexto
	
	@Test
	public void subTextoDevePassar(){
		final Texto texto = new Texto("Uma frase grande.");
		final String subTexto = texto.subText(4, 9);
		
		Assert.assertEquals("Sub texto invalido.", "frase", subTexto);
		
	}
	
	@Test(expected=StringIndexOutOfBoundsException.class)
	public void subTextoForaAlcance(){
		final Texto texto = new Texto("Uma frase grande.");
		texto.subText(10, 20);
		
		Assert.fail("Deveria ter retornado um index out of bound.");
		
	}
	
	@Test(expected=StringIndexOutOfBoundsException.class)
	public void subTextoComIndicesInvertidos(){
		final Texto texto = new Texto("Uma frase grande.");
		texto.subText(20, 10);
		
		Assert.fail("Deveria ter retornado um index out of bound.");
		
	}
	
	@Test(expected=StringIndexOutOfBoundsException.class)
	public void subTextoComIndicesNegativos(){
		final Texto texto = new Texto("Uma frase grande.");
		texto.subText(-1, -2);
		
		Assert.fail("Deveria ter retornado um index out of bound.");
		
	}
	
	// terminaCom
	
	@Test
	public void termicaComDevePassar() {
		final Texto texto = new Texto("Abacaxi");
		
		Assert.assertTrue(texto.terminaCom("xi"));
	}
	
	@Test
	public void terminaComDeveRetornarFalso() {
		final Texto texto = new Texto("Morango");
		
		Assert.assertFalse(texto.terminaCom("123"));
	}
	
	@Test(expected=NullPointerException.class)
	public void terminaComDeveFalhar() {
		final Texto texto = new Texto(null);
		texto.terminaCom("123");
		
		Assert.fail("Deve retornar um NullPointerException.");
	}
	
	// eliminarEspacos
	
	@Test
	public void eliminarEspacosDevePassar() {
		final Texto texto = new Texto("Eliminar espa�os deve passar.");
		final String resultado = texto.eliminarEspacos();
		
		Assert.assertEquals(26, resultado.length());
		Assert.assertEquals("Eliminarespa�osdevepassar.", resultado);
	}
	
	@Test
	public void eliminarEspacosEmTextoSemEspacos() {
		final Texto texto = new Texto("Texto.");
		final String resultado = texto.eliminarEspacos();
		
		Assert.assertEquals(6, resultado.length());
		Assert.assertEquals("Texto.", resultado);
	}
	
	@Test(expected=NullPointerException.class)
	public void eliminarEspacosDeveFalhar() {
		final Texto texto = new Texto(null);
		texto.eliminarEspacos();
		
		Assert.fail("Deve retornar um NullPointerException.");
	}
	
	// caracterNoIndice
	
	@Test
	public void caracterNoIndiceDevePassar() {
		final Texto texto = new Texto("Um grande texto.");
		
		Assert.assertEquals('U', texto.caracterNoIndice(0));
	}
	
	@Test(expected=StringIndexOutOfBoundsException.class)
	public void caracterNoIndiceNegativo() {
		final Texto texto = new Texto("Um texto grande.");
		texto.caracterNoIndice(-1);
		
		Assert.fail("Deve retornar um Index Out of Bounds Exception.");
	}
	
	@Test(expected=NullPointerException.class)
	public void caracterNoIndiceDeveFalhar() {
		final Texto texto = new Texto(null);
		texto.caracterNoIndice(0);
		
		Assert.fail("Deve retornar um Null Pointer Exception.");
	}
	
	// separarPor
	
	@Test
	public void separarPorDevePassar() {
		final Texto texto = new Texto("Um grande texto.");
		final String[] textos = texto.separarPor(" ");
		
		Assert.assertEquals(3, textos.length);
		Assert.assertEquals("Um", textos[0]);
		Assert.assertEquals("grande", textos[1]);
		Assert.assertEquals("texto.", textos[2]);
	}
	
	@Test(expected=AssertionError.class)
	public void separarPorSeparadorNaoEncontrado() {
		final Texto texto = new Texto("Um grande texto.");
		texto.separarPor(",");
		
		Assert.fail("Separador nao encontrado, deve retornar um Assertion Error.");
	}
	
	@Test(expected=NullPointerException.class)
	public void separarPorDeveFalhar() {
		final Texto texto = new Texto("Um grande texto.");
		texto.separarPor(null);
		
		Assert.fail("Deve retornar um Null Pointer Exception.");
	}
	
}
