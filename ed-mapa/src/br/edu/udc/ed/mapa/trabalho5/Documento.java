package br.edu.udc.ed.mapa.trabalho5;

public class Documento {
	
	private int rg;
	private int cpf;
	
	public int getRg() {
		return rg;
	}
	public void setRg(int rg) {
		this.rg = rg;
	}
	public int getCpf() {
		return cpf;
	}
	public void setCpf(int cpf) {
		this.cpf = cpf;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(cpf);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(rg);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Documento other = (Documento) obj;
		if (Double.doubleToLongBits(cpf) != Double.doubleToLongBits(other.cpf))
			return false;
		if (Double.doubleToLongBits(rg) != Double.doubleToLongBits(other.rg))
			return false;
		return true;
	}

}
