package br.edu.udc.ed.mapa.trabalho5.testes;

import org.junit.Assert;
import org.junit.Test;

import br.edu.udc.ed.mapa.Mapa;
import br.edu.udc.ed.mapa.trabalho5.Documento;
import br.edu.udc.ed.mapa.trabalho5.Pessoa;

public class Trabalho5Testes {
	
	@Test
	public void mapaAdicionarDevePassar(){
		final Mapa<Documento, Pessoa> mapa = new Mapa<>();
		
		final Pessoa pessoa = new Pessoa();
		pessoa.setNome("Spiderman");
		final Documento documento = new Documento();
		documento.setCpf(555578988);
		documento.setRg(454567854);
		
		mapa.adiciona(documento, pessoa);
		
		
		Assert.assertEquals(1, mapa.tamanho());
		Assert.assertTrue( mapa.contem(documento) );
	}

}
