package br.edu.udc.ed.mapa.testes;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.junit.Test;

import br.edu.udc.ed.mapa.Heroi;
import br.edu.udc.ed.mapa.heroi.MapaTabelaHeroi;

import org.junit.Assert;

public class MapaTabelaHeroiTestes {
	
	@Test
	public void mapaTabelaHeroiAdicionaDevePassar() {
		final MapaTabelaHeroi mapaTabelaHeroi = new MapaTabelaHeroi();
		final Heroi hulk = new Heroi();
		hulk.setNome("Hulk");
		hulk.setEspecialidade("For�a");
		hulk.setForca(105F);
		hulk.setFraqueza("Bravo");
		hulk.setVoa(false);
		mapaTabelaHeroi.adiciona("Hulks", hulk);
		
		final Heroi homemaranha = new Heroi();
		homemaranha.setNome("Homem Aranha");
		homemaranha.setEspecialidade("Teias");
		homemaranha.setForca(25F);
		homemaranha.setHumano(true);
		homemaranha.setFraqueza("Pobre");
		homemaranha.setVoa(false);
		mapaTabelaHeroi.adiciona("Pobres", homemaranha);

		Assert.assertTrue(mapaTabelaHeroi.contem("Hulks"));
		Assert.assertTrue(mapaTabelaHeroi.contem("Pobres"));
		Assert.assertEquals(mapaTabelaHeroi.tamanho(), 2);
		Assert.assertEquals(hulk, mapaTabelaHeroi.obtem("Hulks"));
		Assert.assertEquals(homemaranha, mapaTabelaHeroi.obtem("Pobres"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void mapaTabelaHeroiRemoveDeveFalhar() {
		final MapaTabelaHeroi mapaTabelaHeroi = new MapaTabelaHeroi();
		mapaTabelaHeroi.remove("Heroi n�o existe.");
	}

	@Test
	public void removeDevePassar() {
		final MapaTabelaHeroi mapaTabelaHeroi = new MapaTabelaHeroi();
		final Heroi hulk = new Heroi();
		hulk.setNome("Hulk");
		hulk.setEspecialidade("For�a");
		hulk.setForca(105F);
		hulk.setFraqueza("Bravo");
		hulk.setVoa(false);

		mapaTabelaHeroi.adiciona("Hulks", hulk);
		mapaTabelaHeroi.remove("Hulks");
		Assert.assertEquals(0, mapaTabelaHeroi.tamanho());
		Assert.assertFalse(mapaTabelaHeroi.contem("Hulks"));
	}
	
	@Test
	public void mapaTabelaHeroisBalanceamento() {
		final MapaTabelaHeroi herois = new MapaTabelaHeroi();

		for(int i = 0; i < 99999; i++) {
			final Heroi heroi = new Heroi();
			heroi.setNome(new BigInteger(20, new SecureRandom()).toString(32));
			herois.adiciona(new BigInteger(20, new SecureRandom()).toString(32), heroi);
		}
		
		herois.imprimir();
		System.out.println(herois.tamanho());
	}

}
