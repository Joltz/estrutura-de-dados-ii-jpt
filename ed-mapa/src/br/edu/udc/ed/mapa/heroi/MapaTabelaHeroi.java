package br.edu.udc.ed.mapa.heroi;

import br.edu.udc.ed.mapa.Heroi;
import br.edu.udc.ed.vetor.Vetor;

public class MapaTabelaHeroi {

	private Vetor<Vetor<AssociacaoHeroi>> tabela = new Vetor<>();
	private int quantidade = 0;
	
	private static final short TAMANHO_MINIMO = 10;

	public MapaTabelaHeroi() {
		for (int i = 0; i < 100; i++) {
			this.tabela.adiciona(new Vetor<AssociacaoHeroi>());
		}
	}

	private int calculaIndiceDaTabela(String qrcode) {
		return Math.abs(qrcode.hashCode()) % this.tabela.tamanho();
	}

	public void adiciona(String qrcode, Heroi heroi) {
		if (this.contem(qrcode)) {
			this.remove(qrcode);
		}
		this.verificaCarga();
		final int indice = this.calculaIndiceDaTabela(qrcode);
		final Vetor<AssociacaoHeroi> vetor = this.tabela.obtem(indice);
		vetor.adiciona(new AssociacaoHeroi(qrcode, heroi));
		this.quantidade++;
	}

	public void remove(String qrcode) {
		final int indice = this.calculaIndiceDaTabela(qrcode);

		final Vetor<AssociacaoHeroi> vetor = this.tabela.obtem(indice);
		for (int i = 0; i < vetor.tamanho(); i++) {
			final AssociacaoHeroi associacao = vetor.obtem(i);
			if (associacao.getQRCode().equals(qrcode)) {
				vetor.remove(i);
				this.quantidade--;
				this.verificaCarga();
				return;
			}
		}
		throw new IllegalArgumentException("N�o existe her�i com este QRCode.");
	}

	public Heroi obtem(String qrcode) {
		final int indice = this.calculaIndiceDaTabela(qrcode);
	    final Vetor<AssociacaoHeroi> vetor = this.tabela.obtem(indice);
	    
	    for (int i = 0; i < vetor.tamanho(); i++) {
	    		final AssociacaoHeroi associacao = vetor.obtem(i);
	        if (associacao.getQRCode().equals(qrcode)) {
	            return associacao.getHeroi();
	        }
	    }
	    
	    throw new IllegalArgumentException("N�o existe her�i com este qrcode.");
	}

	public boolean contem(String qrcode) {
		final int indice = this.calculaIndiceDaTabela(qrcode);

		final Vetor<AssociacaoHeroi> vetor = this.tabela.obtem(indice);
		for (int i = 0; i < vetor.tamanho(); i++) {
			final AssociacaoHeroi associacao = vetor.obtem(i);
			if (associacao.getQRCode().equals(qrcode)) {
				return true;
			}
		}
		return false;
	}

	public int tamanho() {
		return this.quantidade;
	}
	
	private void verificaCarga() {
		final int capacidade = this.tabela.tamanho();
		final double carga = (double) this.quantidade / capacidade;
		
		if(carga > 0.75) {
			this.redimensionaTabela(capacidade * 2);
		} else if(carga < 0.25) {
			final int novaCapacidade = capacidade / 2;
			if(novaCapacidade > TAMANHO_MINIMO) {
				this.redimensionaTabela(novaCapacidade);
			}
		}
	}
	
	private void redimensionaTabela(int novaCapacidade) {
		final Vetor<AssociacaoHeroi> objetos = this.todas();
		this.tabela = new Vetor<>();
		
		for(int i = 0; i < novaCapacidade; i++) {
			this.tabela.adiciona(new Vetor<AssociacaoHeroi>());
		}
		
		for(int i = 0; i < objetos.tamanho(); i++) {
			final AssociacaoHeroi objeto = objetos.obtem(i);
			final int indice = this.calculaIndiceDaTabela(objeto.getQRCode());
			this.tabela.obtem(indice).adiciona(objeto);
		}
	}
	
	public Vetor<AssociacaoHeroi> todas() {
		final Vetor<AssociacaoHeroi> todosObjetos = new Vetor<AssociacaoHeroi>();

		for (int i = 0; i < this.tabela.tamanho(); i++) {
			final Vetor<AssociacaoHeroi> objetos = this.tabela.obtem(i);

			for (int j = 0; j < objetos.tamanho(); j++) {
				final AssociacaoHeroi objeto = objetos.obtem(j);
				todosObjetos.adiciona(objeto);
			}
		}

		return todosObjetos;
	}
	
	public void imprimir() {
		
		System.out.println("CAPACIDADE: " + this.tabela.tamanho());
		
		for (int i = 0; i < this.tabela.tamanho(); i++) {
			final Vetor<AssociacaoHeroi> objetos = this.tabela.obtem(i);
			
			if(objetos.tamanho() == 0) continue;
			
			System.out.println("Codigo: " + i + " --- Total: " + objetos.tamanho());
			
			for (int j = 0; j < objetos.tamanho(); j++) {
				System.out.println(objetos.obtem(j));
			}
		}
	}
	
}
